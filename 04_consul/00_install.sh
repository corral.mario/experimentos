#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x
echo "Installing Consul...."
kubectl create namespace consul
kubectl label namespace consul istio-injection-
#kubectl apply -f Manifest.yaml
helm repo add hashicorp https://helm.releases.hashicorp.com
helm repo update
echo installing consul...

helm install --values consul-values.yaml consul hashicorp/consul --create-namespace --namespace consul --version "1.0.0" -y
kubectl apply -f Manifestgw.yaml
sleep 30

echo "#https://consul.consul.homelab ---- minikube" | sudo tee -a /etc/hosts
echo $(kubectl get svc -n istio-system istio-front-ingressgateway -o jsonpath={.spec.clusterIP}) consul.consul.homelab | sudo tee -a /etc/hosts

echo "Installing Consul....Done. TOKEN:"
kubectl get secret -n consul consul-bootstrap-acl-token -o yaml|grep token:| awk '{print $2}'|base64 --decode
