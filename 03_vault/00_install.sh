#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x
echo "Installing Vault...."
kubectl create namespace vault
kubectl label namespace vault istio-injection=enabled --overwrite
kubectl apply -f Manifest.yaml
echo "Waiting for Vault to start..."
sleep 10
kubectl exec -it -n vault vault-0 -- mkdir /vault/tmp || exit 1

echo "Waiting for Vault to unseal init..."
sleep 5
kubectl exec -it -n vault vault-0 -- sh -c "vault operator init -key-shares=1 -key-threshold=1 > /vault/tmp/vault-init.txt" || exit 1

echo "Waiting for Vault unseal key..."
sleep 5
kubectl exec -it -n vault vault-0 -- sh -c "cat /vault/tmp/vault-init.txt | grep "Unseal Key" | cut -d' ' -f4 > /vault/tmp/vault-unseal-key.txt" || exit 1

echo "Waiting for Vault root token..."
sleep 5
kubectl exec -it -n vault vault-0 -- sh -c "cat /vault/tmp/vault-init.txt | grep "Initial Root Token" | cut -d' ' -f4 > /vault/tmp/vault-root-token.txt" | awk '{print $4}' > .vaultoken|| exit 1

tr -d '\015' <.vaultoken >.vault-token
rm .vaultoken


echo "Unsealing Vault operator..."
sleep 5
kubectl exec -it -n vault vault-0 -- sh -c "vault operator unseal '$(cat /vault/tmp/vault-unseal-key.txt)'" || exit 1


echo "Enabling Vault K/V..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault secrets enable -address="https://vault.vault.homelab" -tls-skip-verify -path=apps kv-v2 || exit 1

echo "adding Vault K/V secrets..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault kv put -address="https://vault.vault.homelab" -tls-skip-verify  apps/nginx/staging/test/config username="static-user" password="static-password" || exit 1

echo "enable kubernetes auth..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault auth enable -address="https://vault.vault.homelab" -tls-skip-verify kubernetes || exit 1

echo "configure kubernetes auth..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault write -address="https://vault.vault.homelab" -tls-skip-verify  auth/kubernetes/config kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443"

echo "create vault policy for read nginx secrets..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault policy write -address="https://vault.vault.homelab" -tls-skip-verify  nginx-secrets - <<EOF
path "apps/nginx/staging/test/config" {
	capabilities = ["read"]
}
EOF

echo "create vault role for nginx-secrets..."
sleep 5
VAULT_TOKEN=$(cat .vault-token) vault write -address="https://vault.vault.homelab" -tls-skip-verify  auth/kubernetes/role/nginx bound_service_account_names=nginx bound_service_account_namespaces=nginx policies=nginx-secrets ttl=24h || exit 1





echo "#https://vault.vault.homelab ---- minikube" | sudo tee -a /etc/hosts
echo $(kubectl get svc -n istio-system istio-front-ingressgateway -o jsonpath={.spec.clusterIP}) vault.vault.homelab | sudo tee -a /etc/hosts

