#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

minikube start --nodes 3 -p multinode-demo --memory=6144 --cpus=2 --kubernetes-version=v1.22.2 --driver=docker --enable-default-cni=false --network-plugin=cni
#minikube tunnel -p multinode-demo  &> /dev/null &
