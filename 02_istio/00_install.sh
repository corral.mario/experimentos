#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

istioctl install --set profile=demo -y -f override.yml

openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -subj '/O=istio Inc./CN=istio.localhost' -keyout istio.localhost.key -out istio.localhost.crt

#Creamos requests csr para la CA
openssl req -out gw.istio.localhost.csr -newkey rsa:2048 -nodes -keyout gw.istio.localhost.key -subj "/CN=gw.istio.localhost/O=localhost"

#Creamos certificado con la csr
openssl x509 -req -sha256 -days 365 -CA istio.localhost.crt -CAkey istio.localhost.key -set_serial 0 -in gw.istio.localhost.csr -out gw.istio.localhost.crt


#Creamos secreto con certificado del server
kubectl create secret tls istio-ingress-cert --key gw.istio.localhost.key --cert gw.istio.localhost.crt -n istio-system

