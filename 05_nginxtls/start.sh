#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -subj '/O=nginx Inc./CN=nginx.homelab' -keyout nginx.homelab.key -out nginx.homelab.crt

#Creamos requests csr para la CA
openssl req -out nginxprueba.nginx.homelab.csr -newkey rsa:2048 -nodes -keyout nginxprueba.nginx.homelab.key -subj "/CN=nginxprueba.nginx.homelab/O=homelab"

#Creamos certificado con la csr
openssl x509 -req -sha256 -days 365 -CA nginx.homelab.crt -CAkey nginx.homelab.key -set_serial 0 -in nginxprueba.nginx.homelab.csr -out nginxprueba.nginx.homelab.crt


#Creamos namespaces de pruebas
kubectl create namespace nginx

#Creamos configmap desde fichero
kubectl create configmap nginx-configmap --from-file=nginx.conf=./nginx.conf -n nginx

#Creamos label para injectar istio en el namespace
kubectl label namespace nginx istio-injection=enabled --overwrite

#Creamos secreto con certificado del server
kubectl create secret tls nginx-server-certs --key nginxprueba.nginx.homelab.key --cert nginxprueba.nginx.homelab.crt -n nginx

#Aplicamos manifiesto:
kubectl apply -f Manifest_nginxtls.yaml

#COmprobamos certificado
kubectl exec -n nginx "$(kubectl get pod -n nginx -l run=my-nginx -o jsonpath={.items..metadata.name})" -c istio-proxy -- curl -sS -v -k --resolve nginxprueba.nginx.homelab:443:127.0.0.1 https://nginxprueba.nginx.homelab


#Añadimos gw y virtualservice

kubectl apply -f Manifest_nginxgw.yaml

echo "#https://nginxprueba.nginx.homelab ---- minikube" | sudo tee -a /etc/hosts
echo $(kubectl get svc -n istio-system istio-front-ingressgateway2 -o jsonpath={.spec.clusterIP}) nginxprueba.nginx.homelab | sudo tee -a /etc/hosts
