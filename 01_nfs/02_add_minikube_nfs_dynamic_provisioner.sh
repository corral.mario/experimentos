#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

kubectl apply -f nfs-subdir-external-provisioner/deploy/rbac.yaml
kubectl apply -f nfs-subdir-external-provisioner/deploy/deployment.yaml
kubectl apply -f nfs-subdir-external-provisioner/deploy/storageClass.yaml
