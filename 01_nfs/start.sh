#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

./00_docker.sh  &&  ./01_add_minikube_network.sh  && 02_add_minikube_nfs_dynamic_provisioner.sh  && 03_add_minikube_nfs_csi_driver.sh

