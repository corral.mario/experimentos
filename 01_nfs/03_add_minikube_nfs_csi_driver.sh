#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

kubectl apply -f csi-driver-nfs/deploy/rbac-csi-nfs-controller.yaml
kubectl apply -f csi-driver-nfs/deploy/csi-nfs-driverinfo.yaml
kubectl apply -f csi-driver-nfs/deploy/csi-nfs-controller.yaml
kubectl apply -f csi-driver-nfs/deploy/csi-nfs-node.yaml
