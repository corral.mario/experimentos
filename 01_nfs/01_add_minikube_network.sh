#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

docker network connect minikube nfs-server 
docker network connect multinode-demo nfs-server 
