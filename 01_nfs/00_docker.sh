#!/usr/bin/env bash

[ "$DEBUG" != "" ] && set -x

mkdir -p nfs/shared
PWD=$(pwd)
docker run -d --rm --privileged --name nfs-server  -v $PWD/nfs/shared:/var/nfs  phico/nfs-server:latest

sudo modprobe nfs
sudo modprobe nfsd
